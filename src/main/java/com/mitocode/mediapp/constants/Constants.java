package com.mitocode.mediapp.constants;

/**
 * @author Mariela Cladera M.
 */
public final class Constants {

    private Constants() {

    }

    public static class ERRORS {
        public static final String ERROR_500 = "INTERNAL SERVICE ERROR";
        public static final String ERROR_400 = "ID NOT FOUND";
    }

    public static class CONSULTATION {
        public static final String CONSULTATION_PATH = "/consultations";
        public static final String CONSULTATION_PATH_ID = "/consultations/{id}";
    }

    public static class DOCTOR {
        public static final String DOCTOR_PATH = "/doctors";
        public static final String DOCTOR_PATH_BY_ID = "/doctors/{id}";
    }

    public static class EXAM {
        public static final String EXAM_PATH = "/exams";
        public static final String EXAM_PATH_ID = "/exams/{id}";
    }

    public static class MENU {
        public static final String MENU_PATH = "/menus";
        public static final String MENU_PATH_ID = "/menus/{id}";
    }

    public static class PATIENT {
        public static final String PATIENT_PATH = "/patients";
        public static final String PATIENT_PATH_BY_ID = "/patients/{id}";
    }

    public static class ROL {
        public static final String ROL_PATH = "/roles";
        public static final String ROL_PATH_ID = "/roles/{id}";
    }

    public static class SPECIALTY {
        public static final String SPECIALTY_PATH = "/specialties";
        public static final String SPECIALTY_PATH_BY_ID = "/specialties/{id}";
    }

    public static class TAGS_OF_CONTROLLERS {
        public static final String CONSULTATION_TAG = "Controllers of Consultation";
        public static final String DOCTOR_TAG = "Controllers of Doctor";
        public static final String EXAM_TAG = "Controllers of Exam";
        public static final String MENU_TAG = "Controllers of Menu";
        public static final String PATIENT_TAG = "Controllers of Patient";
        public static final String ROL_TAG = "Controllers of Rol";
        public static final String SPECIALTY_TAG = "Controllers of Specialty";
    }
}
