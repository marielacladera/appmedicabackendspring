package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.FindPatientByIdCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.PatientResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.PATIENT_TAG)
@RestController
@RequestMapping
public class FindPatientByIdController {
    private FindPatientByIdCmd commander;

    public FindPatientByIdController(FindPatientByIdCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Find a Patient by id")
    @GetMapping(value = Constants.PATIENT.PATIENT_PATH_BY_ID)
    public ResponseEntity<PatientResponse> findPatientById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
