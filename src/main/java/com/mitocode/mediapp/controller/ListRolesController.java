package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.ListRolesCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.RolResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.ROL_TAG)
@RestController
@RequestScope
public class ListRolesController {
    private ListRolesCmd commander;

    public ListRolesController(ListRolesCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "List all the roles")
    @GetMapping(value = Constants.ROL.ROL_PATH)
    public ResponseEntity<List<RolResponse>> listRoles() {
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
