package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreateMenuCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.MenuCreateInput;
import com.mitocode.mediapp.response.MenuResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.MENU_TAG)
@RestController
@RequestScope
public class CreateMenuController {
    private CreateMenuCmd commander;

    public CreateMenuController(CreateMenuCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Menu")
    @PostMapping(value = Constants.MENU.MENU_PATH)
    public ResponseEntity<MenuResponse> createMenu(@Valid @RequestBody MenuCreateInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.CREATED);
    }

}
