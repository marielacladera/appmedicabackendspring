package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.ListConsultationsCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.ConsultationResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.CONSULTATION_TAG)
@RestController
@RequestScope
public class ListConsultationsController {
    private ListConsultationsCmd commander;

    public ListConsultationsController(ListConsultationsCmd commander){
        this.commander = commander;
    }

    @Operation(summary = "List all the Medical Consultations")
    @GetMapping(Constants.CONSULTATION.CONSULTATION_PATH)
    public ResponseEntity<List<ConsultationResponse>> listConsultations(){
        commander.execute();
        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
