package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreateConsultationCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.input.ConsultationListExamCreateInput;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.CONSULTATION_TAG)
@RestController
@RequestScope
public class CreateConsultationController {

    private CreateConsultationCmd commander;

    public CreateConsultationController(CreateConsultationCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Consultation of patient")
    @PostMapping(value = Constants.CONSULTATION.CONSULTATION_PATH)
    public ResponseEntity<Void> createConsultation(@Valid @RequestBody ConsultationListExamCreateInput request) {
        commander.setRequest(request);
        commander.execute();
        URI location  = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(commander.getResponse().getConsultationId()).toUri();
        return ResponseEntity.created(location).build();
    }

}
