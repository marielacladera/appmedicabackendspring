package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.UpdatePatientCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.PatientInput;
import com.mitocode.mediapp.response.PatientResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.PATIENT_TAG)
@RestController
@RequestScope
public class UpdatePatientController {
    private UpdatePatientCmd commander;

    public UpdatePatientController(UpdatePatientCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Update information of a Patient")
    @PutMapping(value = Constants.PATIENT.PATIENT_PATH)
    public ResponseEntity<PatientResponse> updatePatient(@Valid @RequestBody PatientInput patient) {
        commander.setRequest(patient);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
