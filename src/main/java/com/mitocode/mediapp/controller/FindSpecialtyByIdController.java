package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.FindSpecialtyByIdCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.SpecialtyResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.SPECIALTY_TAG)
@RestController
@RequestMapping
public class FindSpecialtyByIdController {
    private FindSpecialtyByIdCmd commander;

    public FindSpecialtyByIdController(FindSpecialtyByIdCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Find a specialty by id")
    @GetMapping(value = Constants.SPECIALTY.SPECIALTY_PATH_BY_ID)
    public ResponseEntity<SpecialtyResponse> findSpecialtyById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
