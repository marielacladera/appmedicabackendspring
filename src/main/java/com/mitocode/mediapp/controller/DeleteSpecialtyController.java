package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.DeleteSpecialtyCmd;
import com.mitocode.mediapp.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.SPECIALTY_TAG)
@RestController
@RequestScope
public class DeleteSpecialtyController {
    private DeleteSpecialtyCmd commander;

    public DeleteSpecialtyController(DeleteSpecialtyCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Delete a Specialty by id")
    @DeleteMapping(value = Constants.SPECIALTY.SPECIALTY_PATH_BY_ID)
    public ResponseEntity<Void> findSpecialtyById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
