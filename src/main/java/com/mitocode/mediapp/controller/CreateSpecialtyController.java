package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreateSpecialtyCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.SpecialtyCreateInput;
import com.mitocode.mediapp.response.SpecialtyResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.SPECIALTY_TAG)
@RestController
@RequestScope
public class CreateSpecialtyController {
    private CreateSpecialtyCmd commander;

    public CreateSpecialtyController(CreateSpecialtyCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Specialty")
    @PostMapping(value = Constants.SPECIALTY.SPECIALTY_PATH)
    public ResponseEntity<SpecialtyResponse> createSpecialty(@Valid @RequestBody SpecialtyCreateInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.CREATED);
    }

}
