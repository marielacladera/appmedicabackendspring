package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreateRolCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.RolCreateInput;
import com.mitocode.mediapp.response.RolResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.ROL_TAG)
@RestController
@RequestScope
public class CreateRolController {
    private CreateRolCmd commander;

    public CreateRolController(CreateRolCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Rol")
    @PostMapping(value = Constants.ROL.ROL_PATH)
    public ResponseEntity<RolResponse> createRol(@Valid @RequestBody RolCreateInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.CREATED);
    }

}
