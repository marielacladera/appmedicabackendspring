package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.ListExamsCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.ExamResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.EXAM_TAG)
@RestController
@RequestScope
public class ListExamsController {
    private ListExamsCmd commander;

    public ListExamsController(ListExamsCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "List all the exams")
    @GetMapping(value = Constants.EXAM.EXAM_PATH)
    public ResponseEntity<List<ExamResponse>> listExams() {
       commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
