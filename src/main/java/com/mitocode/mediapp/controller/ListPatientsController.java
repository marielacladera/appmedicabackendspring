package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.ListPatientsCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.PatientResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.PATIENT_TAG)
@RestController
@RequestScope
public class ListPatientsController {
    private ListPatientsCmd commander;

    public ListPatientsController(ListPatientsCmd commander){
        this.commander = commander;
    }

    @Operation(summary = "List all the patients")
    @GetMapping(value = Constants.PATIENT.PATIENT_PATH)
    public ResponseEntity<List<PatientResponse>> listPatients() {
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
