package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.FindConsultationByIdCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.ConsultationResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RestController
@RequestScope
public class FindConsultationByIdController {

    private FindConsultationByIdCmd commander;

    public FindConsultationByIdController(FindConsultationByIdCmd commander) {
        this.commander = commander;
    }

    @GetMapping(Constants.CONSULTATION.CONSULTATION_PATH_ID)
    public ResponseEntity<ConsultationResponse> findConsultationById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
