package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.UpdateRolCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.RolInput;
import com.mitocode.mediapp.response.RolResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.ROL_TAG)
@RestController
@RequestScope
public class UpdateRolController {
    private UpdateRolCmd commander;

    public UpdateRolController(UpdateRolCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Update information of a Rol")
    @PutMapping(value = Constants.ROL.ROL_PATH)
    public ResponseEntity<RolResponse> updateRol(@Valid @RequestBody RolInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
