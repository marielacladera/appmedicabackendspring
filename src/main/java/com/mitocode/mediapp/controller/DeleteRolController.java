package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.DeleteRolCmd;
import com.mitocode.mediapp.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.ROL_TAG)
@RestController
@RequestMapping
public class DeleteRolController {
    private DeleteRolCmd commander;

    public DeleteRolController(DeleteRolCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Delete a Rol by id")
    @DeleteMapping(value = Constants.ROL.ROL_PATH_ID)
    public ResponseEntity<Void> deleteRol(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
