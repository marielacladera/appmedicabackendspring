package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.ListMenusCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.MenuResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.MENU_TAG)
@RestController
@RequestScope
public class ListMenusController {
    private ListMenusCmd commander;

    public ListMenusController(ListMenusCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "List all the menus")
    @GetMapping(value = Constants.MENU.MENU_PATH)
    public ResponseEntity<List<MenuResponse>> listMenus() {
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
