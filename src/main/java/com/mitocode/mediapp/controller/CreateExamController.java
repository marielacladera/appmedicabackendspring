package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreateExamCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.ExamCreateInput;
import com.mitocode.mediapp.response.ExamResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.EXAM_TAG)
@RestController
@RequestScope
public class CreateExamController {
    private CreateExamCmd commander;

    public CreateExamController(CreateExamCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Exam")
    @PostMapping(value = Constants.EXAM.EXAM_PATH)
    public ResponseEntity<ExamResponse> createExam(@Valid @RequestBody ExamCreateInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.CREATED);
    }

}
