package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreateDoctorCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.DoctorCreateInput;
import com.mitocode.mediapp.response.DoctorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.DOCTOR_TAG)
@RestController
@RequestScope
public class CreateDoctorController {
    private CreateDoctorCmd commander;

    public CreateDoctorController(CreateDoctorCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Doctor")
    @PostMapping(value = Constants.DOCTOR.DOCTOR_PATH)
    public ResponseEntity<DoctorResponse> createDoctor(@Valid @RequestBody DoctorCreateInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.CREATED);
    }

}
