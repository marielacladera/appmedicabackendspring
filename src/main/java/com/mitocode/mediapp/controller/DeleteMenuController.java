package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.DeleteMenuCmd;
import com.mitocode.mediapp.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.MENU_TAG)
@RestController
@RequestMapping
public class DeleteMenuController {
    private DeleteMenuCmd commander;

    public DeleteMenuController(DeleteMenuCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Delete a Menu by id")
    @DeleteMapping(value = Constants.MENU.MENU_PATH_ID)
    public ResponseEntity<Void> deleteMenu(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
