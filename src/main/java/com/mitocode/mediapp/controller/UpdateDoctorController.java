package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.UpdateDoctorCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.DoctorInput;
import com.mitocode.mediapp.response.DoctorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.DOCTOR_TAG)
@RestController
@RequestScope
public class UpdateDoctorController {
    private UpdateDoctorCmd commander;

    public UpdateDoctorController(UpdateDoctorCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Update information of a Doctor")
    @PutMapping(value = Constants.DOCTOR.DOCTOR_PATH)
    public ResponseEntity<DoctorResponse> updateDoctor(@Valid @RequestBody DoctorInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(),HttpStatus.OK);
    }

}
