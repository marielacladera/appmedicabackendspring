package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.DeleteExamCmd;
import com.mitocode.mediapp.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.EXAM_TAG)
@RestController
@RequestScope
public class DeleteExamController {
    private DeleteExamCmd commander;

    public DeleteExamController(DeleteExamCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Delete a Exam by id")
    @DeleteMapping(value = Constants.EXAM.EXAM_PATH_ID)
    public ResponseEntity<Void> deleteExam(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

