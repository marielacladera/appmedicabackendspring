package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.ListDoctorsCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.DoctorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.DOCTOR_TAG)
@RestController
@RequestScope
public class ListDoctorsController {
    private ListDoctorsCmd commander;

    public ListDoctorsController(ListDoctorsCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "List all the doctors")
    @GetMapping(value = Constants.DOCTOR.DOCTOR_PATH)
    public ResponseEntity<List<DoctorResponse>> listDoctors() {
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
