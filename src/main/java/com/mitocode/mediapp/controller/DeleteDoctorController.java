package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.DeleteDoctorCmd;
import com.mitocode.mediapp.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.DOCTOR_TAG)
@RestController
@RequestScope
public class DeleteDoctorController {
    private DeleteDoctorCmd commander;

    public DeleteDoctorController(DeleteDoctorCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Delete a Doctor by id")
    @DeleteMapping(value = Constants.DOCTOR.DOCTOR_PATH_BY_ID)
    public ResponseEntity<Void> deleteDoctor(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

