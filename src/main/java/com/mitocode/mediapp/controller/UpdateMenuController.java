package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.UpdateMenuCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.MenuInput;
import com.mitocode.mediapp.response.MenuResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.MENU_TAG)
@RestController
@RequestScope
public class UpdateMenuController {
    private UpdateMenuCmd commander;

    public UpdateMenuController(UpdateMenuCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Update information of a Menu")
    @PutMapping(value = Constants.MENU.MENU_PATH)
    public ResponseEntity<MenuResponse> updateMenu(@Valid @RequestBody MenuInput request) {
        commander.setRequest(request);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
