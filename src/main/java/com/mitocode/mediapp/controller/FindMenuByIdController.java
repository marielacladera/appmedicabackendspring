package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.FindMenuByIdCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.MenuResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.MENU_TAG)
@RestController
@RequestMapping
public class FindMenuByIdController {
    private FindMenuByIdCmd commander;

    public FindMenuByIdController(FindMenuByIdCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Find a Menu by id")
    @GetMapping(value = Constants.MENU.MENU_PATH_ID)
    public ResponseEntity<MenuResponse> findMenuById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
