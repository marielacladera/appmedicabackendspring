package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.CreatePatientCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.PatientCreateInput;
import com.mitocode.mediapp.response.PatientResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.PATIENT_TAG)
@RestController
@RequestScope
public class CreatePatientController {

    private CreatePatientCmd commander;

    public CreatePatientController(CreatePatientCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Create a Patient")
    @PostMapping(value = Constants.PATIENT.PATIENT_PATH)
    public ResponseEntity<PatientResponse> createPatient(@RequestBody PatientCreateInput patient) {
        commander.setRequest(patient);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.CREATED);
    }

}
