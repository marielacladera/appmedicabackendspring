package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.DeletePatientCmd;
import com.mitocode.mediapp.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.PATIENT_TAG)
@RestController
@RequestMapping
public class DeletePatientController {
    private DeletePatientCmd commander;

    public DeletePatientController(DeletePatientCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Delete a Patient by id")
    @DeleteMapping(value = Constants.PATIENT.PATIENT_PATH_BY_ID)
    public ResponseEntity<Void> findPatientById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
