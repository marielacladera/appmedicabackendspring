package com.mitocode.mediapp.controller;

import com.mitocode.mediapp.commander.FindRolByIdCmd;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.RolResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS_OF_CONTROLLERS.ROL_TAG)
@RestController
@RequestMapping
public class FindRolByIdController {
    private FindRolByIdCmd commander;

    public FindRolByIdController(FindRolByIdCmd commander) {
        this.commander = commander;
    }

    @Operation(summary = "Find a Rol by id")
    @GetMapping(value = Constants.ROL.ROL_PATH_ID)
    public ResponseEntity<RolResponse> findRolById(@PathVariable("id") Integer id) {
        commander.setId(id);
        commander.execute();

        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }

}
