package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.domain.ConsultationExam;
import com.mitocode.mediapp.domain.Exam;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Mariela Cladera M.
 */
public interface ConsultationExamRepository extends IGenericRepo<ConsultationExam, Integer> {

    /*@Modifying
    @Query(value = "INSERT INTO consultation_exam(consultation_id, exam_id) VALUES(:consultationId, :examId)", nativeQuery = true)
    Integer registerConsultationExam(Integer consultationId, Integer examId);*/
}
