package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Usuario;

/**
 * @author Mariela Cladera M.
 */
public interface UserRepository extends IGenericRepo<Usuario, Integer> {
}
