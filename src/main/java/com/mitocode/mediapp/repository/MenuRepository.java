package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Menu;
/**
 * @author Mariela Cladera M.
 */
public interface MenuRepository extends IGenericRepo<Menu, Integer> {
}
