package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Doctor;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Mariela Cladera M.
 */
public interface DoctorRepository extends IGenericRepo<Doctor, Integer> {
    @Modifying
    @Transactional
    @Query("update Doctor d set d.names = :names, d.lastName = :lastName, d.urlPhoto = :urlPhoto where d.doctorId = :doctorId")
    void updateValuesDoctor(Integer doctorId, String names, String lastName, String urlPhoto);
    //void updateValuesDoctor(@Param("doctorId") Integer doctorId, @Param("names")String names, @Param("lastName") String lastName, @Param("urlPhoto") String urlPhoto);
}
