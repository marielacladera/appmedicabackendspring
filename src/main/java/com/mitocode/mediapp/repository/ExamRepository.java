package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Exam;

/**
 * @author Mariela Cladera M.
 */
public interface ExamRepository extends IGenericRepo<Exam, Integer> {
}
