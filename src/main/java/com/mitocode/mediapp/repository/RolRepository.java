package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Rol;

/**
 * @author Mariela Cladera M.
 */
public interface RolRepository extends IGenericRepo<Rol, Integer> {
}
