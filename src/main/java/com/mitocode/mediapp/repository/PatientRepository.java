package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Patient;
/**
 * @author Mariela Cladera M.
 */
public interface PatientRepository extends IGenericRepo<Patient, Integer> {
}
