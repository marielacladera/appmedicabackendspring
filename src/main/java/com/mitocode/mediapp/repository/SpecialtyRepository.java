package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Specialty;
/**
 * @author Mariela Cladera M.
 */
public interface SpecialtyRepository extends IGenericRepo<Specialty, Integer> {
}
