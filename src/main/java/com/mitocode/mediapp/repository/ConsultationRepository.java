package com.mitocode.mediapp.repository;

import com.mitocode.mediapp.domain.Consultation;

/**
 * @author Mariela Cladera M.
 */
public interface ConsultationRepository extends IGenericRepo<Consultation, Integer> {
}
