package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.input.ConsultationCreateInput;
import com.mitocode.mediapp.response.ConsultationResponse;

import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
public class ComposeConsultation {

    public static Consultation composeConsultation(ConsultationCreateInput consultation) {
        Consultation instance = new Consultation();
        instance.setDoctor(ComposeDoctor.composeDoctor(consultation.getDoctor()));
        instance.setPatient(ComposePatient.composePatient(consultation.getPatient()));
        instance.setSpecialty(ComposeSpecialty.composeSpecialty(consultation.getSpecialty()));
        instance.setDate(consultation.getDate());
        instance.setOfficeNumber(consultation.getOfficeNumber());
        instance.setDetailConsultation(consultation.getDetailsConsultation()
                .stream().map(detail -> ComposeDetailConsultation.composeDetailConsultation(detail))
                .collect(Collectors.toList()));

        return instance;
    }

    public static ConsultationResponse buildConsultationResponse(Consultation consultation) {
        ConsultationResponse instance = new ConsultationResponse();
        instance.setConsultationId(consultation.getConsultationId());
        instance.setDoctor(ComposeDoctor.buildDoctorResponse(consultation.getDoctor()));
        instance.setPatient(ComposePatient.buildPatientResponse(consultation.getPatient()));
        instance.setSpecialty(ComposeSpecialty.buildSpecialtyResponse(consultation.getSpecialty()));
        instance.setDate(consultation.getDate());
        instance.setOfficeNumber(consultation.getOfficeNumber());
        instance.setDetailsConsultation(consultation.getDetailConsultation()
                .stream().map(detail -> ComposeDetailConsultation.buildDetailConsultationResponse(detail))
                .collect(Collectors.toList()));

        return instance;
    }
}
