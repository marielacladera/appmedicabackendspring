package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Doctor;
import com.mitocode.mediapp.input.DoctorCreateInput;
import com.mitocode.mediapp.input.DoctorInput;
import com.mitocode.mediapp.response.DoctorResponse;

/**
 * @author Mariela Cladera M.
 */
public class ComposeDoctor {
    public static Doctor composeCreateDoctor(DoctorCreateInput doctor) {
        Doctor instance = new Doctor();
        instance.setNames(doctor.getNames());
        instance.setLastName(doctor.getLastName());
        instance.setCmp(doctor.getCmp());
        instance.setUrlPhoto(doctor.getUrlPhoto());

        return instance;
    }

    public static Doctor composeDoctor(DoctorInput doctor) {
        Doctor instance = new Doctor();
        instance.setDoctorId(doctor.getDoctorId());
        instance.setNames(doctor.getNames());
        instance.setLastName(doctor.getLastName());
        //instance.setCmp(doctor.getCmp());
        instance.setUrlPhoto(doctor.getUrlPhoto());
        return instance;
    }

    public static DoctorResponse buildDoctorResponse(Doctor doctor) {
        DoctorResponse instance = new DoctorResponse();
        instance.setDoctorId(doctor.getDoctorId());
        instance.setNames(doctor.getNames());
        instance.setLastName(doctor.getLastName());
        instance.setCmp(doctor.getCmp());
        instance.setUrlPhoto(doctor.getUrlPhoto());

        return instance;
    }

}
