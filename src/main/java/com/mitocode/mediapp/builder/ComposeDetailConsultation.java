package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.DetailConsultation;
import com.mitocode.mediapp.input.DetailConsultationInput;
import com.mitocode.mediapp.response.DetailConsultationResponse;

/**
 * @author Mariela Cladera M.
 */
public class ComposeDetailConsultation {

    public static DetailConsultation composeDetailConsultation(DetailConsultationInput detailConsultation) {
        DetailConsultation instance = new DetailConsultation();
        instance.setDiagnosis(detailConsultation.getDiagnosis());
        instance.setTreatment(detailConsultation.getTreatment());

        return instance;
    }

    public static DetailConsultationResponse buildDetailConsultationResponse(DetailConsultation detailConsultation) {
        DetailConsultationResponse instance = new DetailConsultationResponse();
        instance.setDetailId(detailConsultation.getDetailId());
        instance.setDiagnosis(detailConsultation.getDiagnosis());
        instance.setTreatment(detailConsultation.getTreatment());

        return instance;
    }
}
