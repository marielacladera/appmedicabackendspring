package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Specialty;
import com.mitocode.mediapp.input.SpecialtyCreateInput;
import com.mitocode.mediapp.input.SpecialtyInput;
import com.mitocode.mediapp.response.SpecialtyResponse;

/**
 * @author Mariela Cladera M.
 */
public class ComposeSpecialty {

    public static Specialty composeCreateSpecialty(SpecialtyCreateInput specialty) {
        Specialty instance = new Specialty();
        instance.setName(specialty.getName());
        instance.setDescription(specialty.getDescription());

        return instance;
    }


    public static Specialty composeSpecialty(SpecialtyInput specialty) {
        Specialty instance = new Specialty();
        instance.setSpecialtyId(specialty.getSpecialtyId());
        instance.setName(specialty.getName());
        instance.setDescription(specialty.getDescription());

        return instance;
    }

    public static SpecialtyResponse buildSpecialtyResponse(Specialty specialty) {
        SpecialtyResponse instance = new SpecialtyResponse();
        instance.setSpecialtyId(specialty.getSpecialtyId());
        instance.setName(specialty.getName());
        instance.setDescription(specialty.getDescription());

        return instance;
    }
}
