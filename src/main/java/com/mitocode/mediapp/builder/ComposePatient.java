package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Patient;
import com.mitocode.mediapp.input.PatientCreateInput;
import com.mitocode.mediapp.input.PatientInput;
import com.mitocode.mediapp.response.PatientResponse;

/**
 * @author Mariela Cladera M.
 */
public class ComposePatient {
    public static Patient composeCreatePatient(PatientCreateInput patient) {
        Patient instance = new Patient();
        instance.setNames(patient.getNames());
        instance.setLastName(patient.getLastName());
        instance.setDni(patient.getDni());
        instance.setAddress(patient.getAddress());
        instance.setPhone(patient.getPhone());
        instance.setEmail(patient.getEmail());

        return instance;
    }

    public static Patient composePatient(PatientInput patient) {
        Patient instance = new Patient();
        instance.setPatientId(patient.getPatientId());
        instance.setNames(patient.getNames());
        instance.setLastName(patient.getLastName());
        instance.setDni(patient.getDni());
        instance.setAddress(patient.getAddress());
        instance.setPhone(patient.getPhone());
        instance.setEmail(patient.getEmail());

        return instance;
    }

    public static PatientResponse buildPatientResponse(Patient patient) {
        PatientResponse instance = new PatientResponse();
        instance.setPatientId(patient.getPatientId());
        instance.setNames(patient.getNames());
        instance.setLastName(patient.getLastName());
        instance.setDni(patient.getDni());
        instance.setAddress(patient.getAddress());
        instance.setPhone(patient.getPhone());
        instance.setEmail(patient.getEmail());

        return instance;
    }

}
