package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Exam;
import com.mitocode.mediapp.input.ExamCreateInput;
import com.mitocode.mediapp.input.ExamInput;
import com.mitocode.mediapp.response.ExamResponse;

/**
 * @author Mariela Cladera M.
 */
public class ComposeExam {

    public static Exam composeCreateExam(ExamCreateInput exam) {
        Exam instance = new Exam();
        instance.setName(exam.getName());
        instance.setDescription(exam.getDescription());
        return instance;
    }

    public static Exam composeExam(ExamInput exam) {
        Exam instance = new Exam();
        instance.setExamId(exam.getExamId());
        instance.setName(exam.getName());
        instance.setDescription(exam.getDescription());
        return instance;
    }

    public static ExamResponse builExamResponse(Exam exam) {
        ExamResponse instance = new ExamResponse();
        instance.setExamId(exam.getExamId());
        instance.setName(exam.getName());
        instance.setDescription(exam.getDescription());
        return instance;
    }
}
