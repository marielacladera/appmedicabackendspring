package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Menu;
import com.mitocode.mediapp.input.MenuCreateInput;
import com.mitocode.mediapp.input.MenuInput;
import com.mitocode.mediapp.response.MenuResponse;

import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
public class ComposeMenu {

    public static Menu composeCreateMenu(MenuCreateInput menu) {
        Menu instance = new Menu();
        instance.setName(menu.getName());
        instance.setIcon(menu.getIcon());
        instance.setUrl(menu.getUrl());
        instance.setRoles(menu.getRoles().stream().map(r -> {
            return ComposeRol.composeRol(r);
        }).collect(Collectors.toList()));

        return instance;
    }

    public static Menu composeMenu(MenuInput menu) {
        Menu instance = new Menu();
        instance.setMenuId(menu.getMenuId());
        instance.setName(menu.getName());
        instance.setIcon(menu.getIcon());
        instance.setUrl(menu.getUrl());
        instance.setRoles(menu.getRoles().stream().map(r -> ComposeRol.composeRol(r))
                .collect(Collectors.toList()));

        return instance;
    }

    public static MenuResponse buildMenuResponse(Menu menu) {
        MenuResponse instance = new MenuResponse();
        instance.setMenuId(menu.getMenuId());
        instance.setName(menu.getName());
        instance.setIcon(menu.getIcon());
        instance.setUrl(menu.getUrl());
        instance.setRoles(menu.getRoles().stream().map(r -> ComposeRol.builRolResponse(r))
                .collect(Collectors.toList()));

        return instance;
    }

}
