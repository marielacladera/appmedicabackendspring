package com.mitocode.mediapp.builder;

import com.mitocode.mediapp.domain.Rol;
import com.mitocode.mediapp.input.RolCreateInput;
import com.mitocode.mediapp.input.RolInput;
import com.mitocode.mediapp.response.RolResponse;

/**
 * @author Mariela Cladera M.
 */
public class ComposeRol {

    public static Rol composeCreateRol(RolCreateInput rol) {
        Rol instance = new Rol();
        instance.setName(rol.getName());
        instance.setDescription(rol.getDescription());

        return instance;
    }

    public static Rol composeRol(RolInput rol) {
        Rol instance = new Rol();
        instance.setRolId(rol.getRolId());
        instance.setName(rol.getName());
        instance.setDescription(rol.getDescription());

        return instance;
    }

    public static RolResponse builRolResponse(Rol rol) {
        RolResponse instance = new RolResponse();
        instance.setRolId(rol.getRolId());
        instance.setName(rol.getName());
        instance.setDescription(rol.getDescription());

        return instance;
    }

}
