package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Usuario;

/**
 * @author Mariela Cladera M.
 */
public interface IUserService extends ICRUD<Usuario, Integer> {
}
