package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Patient;

/**
 * @author Mariela Cladera M.
 */
public interface IPatientService extends ICRUD<Patient, Integer> {
}
