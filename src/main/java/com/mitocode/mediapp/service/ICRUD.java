package com.mitocode.mediapp.service;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public interface ICRUD<T, ID> {

    public T create(T t);
    public T update(T t);
    public List<T> list();
    public T findById(ID id);
    public void delete(ID id);
    public boolean exist(ID id);

}
