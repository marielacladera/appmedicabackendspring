package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Rol;

/**
 * @author Mariela Cladera M.
 */
public interface IRolService extends ICRUD<Rol, Integer>{
}
