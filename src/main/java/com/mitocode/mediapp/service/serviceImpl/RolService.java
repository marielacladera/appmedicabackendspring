package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Rol;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.repository.RolRepository;
import com.mitocode.mediapp.service.IRolService;
import org.springframework.stereotype.Service;

/**
 * @author Mariela Cladera M.
 */
@Service
public class RolService extends CRUD<Rol, Integer> implements IRolService {

    private RolRepository repository;

    public RolService(RolRepository repository) {
        this.repository = repository;
    }

    @Override
    protected IGenericRepo<Rol, Integer> getRepo() {
        return repository;
    }
}
