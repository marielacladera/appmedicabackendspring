package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.service.ICRUD;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public abstract class CRUD<T, ID> implements ICRUD<T, ID> {
    protected abstract IGenericRepo<T,ID> getRepo();

    @Override
    public T create(T t) {
        return getRepo().save(t);
    }

    @Override
    public T update(T t) {
        return getRepo().save(t);
    }

    @Override
    public List<T> list() {
        return getRepo().findAll();
    }

    @Override
    public T findById(ID id) {
        return getRepo().findById(id).orElse(null);
    }

    @Override
    public void delete(ID id) {
        getRepo().deleteById(id);
    }

    @Override
    public boolean exist(ID id) {
        return getRepo().existsById(id);
    }

}
