package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.domain.ConsultationExam;
import com.mitocode.mediapp.domain.Exam;
import com.mitocode.mediapp.repository.ConsultationExamRepository;
import com.mitocode.mediapp.repository.ConsultationRepository;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.service.IConsultationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class ConsultationService extends CRUD<Consultation, Integer> implements IConsultationService {

    private ConsultationRepository repository;

    private ConsultationExamRepository ceRepository;

    public ConsultationService(ConsultationRepository repository, ConsultationExamRepository ceRepository) {
        this.repository = repository;
        this.ceRepository = ceRepository;
    }

    @Override
    protected IGenericRepo<Consultation, Integer> getRepo() {
        return repository;
    }

    @Transactional
    @Override
    public Consultation recordTransaction(Consultation consultation, List<Exam> examList) {
        consultation.getDetailConsultation().forEach(detail -> detail.setConsultation(consultation));
        getRepo().save(consultation);
        examList.forEach(exam -> {
            ConsultationExam  consultationExam = new ConsultationExam();
            consultationExam.setConsultation(consultation);
            consultationExam.setExam(exam);

            ceRepository.save(consultationExam);
        });
        return consultation;
    }
}
