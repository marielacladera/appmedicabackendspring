package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Usuario;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.repository.UserRepository;
import com.mitocode.mediapp.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * @author Mariela Cladera M.
 */
@Service
public class UserService extends CRUD<Usuario, Integer> implements IUserService {

    private UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    protected IGenericRepo<Usuario, Integer> getRepo() {
        return repository;
    }
}
