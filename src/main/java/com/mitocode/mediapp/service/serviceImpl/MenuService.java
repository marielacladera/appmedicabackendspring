package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Menu;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.repository.MenuRepository;
import com.mitocode.mediapp.service.IMenuService;
import org.springframework.stereotype.Service;

/**
 * @author Mariela Cladera M.
 */
@Service
public class MenuService extends CRUD<Menu, Integer> implements IMenuService {

    private MenuRepository repository;

    public MenuService(MenuRepository repository) {
        this.repository = repository;
    }

    @Override
    protected IGenericRepo<Menu, Integer> getRepo() {
        return repository;
    }
}
