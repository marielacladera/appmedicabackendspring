package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Doctor;
import com.mitocode.mediapp.repository.DoctorRepository;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.service.IDoctorService;
import org.springframework.stereotype.Service;

/**
 * @author Mariela Cladera M.
 */
@Service
public class DoctorService extends CRUD<Doctor, Integer> implements IDoctorService {

    private DoctorRepository repository;

    public DoctorService(DoctorRepository repository) {
        this.repository = repository;
    }

    @Override
    protected IGenericRepo<Doctor, Integer> getRepo() {
        return repository;
    }

    @Override
    public Doctor updateValuesDoctor(Doctor doctor) {
        repository
                .updateValuesDoctor(doctor.getDoctorId(), doctor.getNames(),
                        doctor.getLastName(), doctor.getUrlPhoto());
        return repository.findById(doctor.getDoctorId()).orElse(null);
    }

}
