package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Specialty;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.repository.SpecialtyRepository;
import com.mitocode.mediapp.service.ISpecialtyService;
import org.springframework.stereotype.Service;

/**
 * @author Mariela Cladera M.
 */
@Service
public class SpecialtyService extends CRUD<Specialty, Integer> implements ISpecialtyService {

    private SpecialtyRepository repository;

    public SpecialtyService(SpecialtyRepository repository) {
        this.repository = repository;
    }

    @Override
    protected IGenericRepo<Specialty, Integer> getRepo() {
        return repository;
    }

}
