package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Patient;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.repository.PatientRepository;
import com.mitocode.mediapp.service.IPatientService;
import org.springframework.stereotype.Service;

/**
 * @author Mariela Cladera M.
 */
@Service
public class PatientService extends CRUD<Patient, Integer> implements IPatientService {

    private PatientRepository repository;

    public PatientService(PatientRepository repository) {
        this.repository = repository;
    }

    @Override
    protected IGenericRepo<Patient, Integer> getRepo() {
        return repository;
    }

}
