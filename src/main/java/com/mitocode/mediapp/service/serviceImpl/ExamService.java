package com.mitocode.mediapp.service.serviceImpl;

import com.mitocode.mediapp.domain.Exam;
import com.mitocode.mediapp.repository.ExamRepository;
import com.mitocode.mediapp.repository.IGenericRepo;
import com.mitocode.mediapp.service.IExamService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class ExamService extends CRUD<Exam, Integer> implements IExamService {

    private ExamRepository repository;

    public ExamService(ExamRepository repository) {
        this.repository = repository;
    }


    @Override
    protected IGenericRepo<Exam, Integer> getRepo() {
        return repository;
    }
}
