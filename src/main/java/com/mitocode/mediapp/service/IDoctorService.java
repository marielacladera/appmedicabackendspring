package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Doctor;

/**
 * @author Mariela Cladera M.
 */
public interface IDoctorService extends ICRUD<Doctor, Integer> {

    Doctor updateValuesDoctor(Doctor doctor);
}
