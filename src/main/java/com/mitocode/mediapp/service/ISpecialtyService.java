package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Specialty;

/**
 * @author Mariela Cladera M.
 */
public interface ISpecialtyService extends ICRUD<Specialty, Integer> {
}
