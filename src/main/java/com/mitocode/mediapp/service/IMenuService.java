package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Menu;

/**
 * @author Mariela Cladera M.
 */
public interface IMenuService extends ICRUD<Menu, Integer>{
}
