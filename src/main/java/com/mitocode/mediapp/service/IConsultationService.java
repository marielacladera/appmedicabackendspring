package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.domain.Exam;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public interface IConsultationService extends ICRUD<Consultation, Integer> {
    Consultation recordTransaction(Consultation consultation, List<Exam> examList);
}
