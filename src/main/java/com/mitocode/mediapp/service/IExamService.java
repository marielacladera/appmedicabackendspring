package com.mitocode.mediapp.service;

import com.mitocode.mediapp.domain.Exam;

/**
 * @author Mariela Cladera M.
 */
public interface IExamService extends ICRUD<Exam, Integer> {
}
