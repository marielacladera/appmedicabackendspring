package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeRol;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.RolInput;
import com.mitocode.mediapp.response.RolResponse;
import com.mitocode.mediapp.service.IRolService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateRolCmd implements BusinessLogicCommand {
    private IRolService service;

    @Setter
    private RolInput request;

    @Getter
    private RolResponse response;

    public UpdateRolCmd(IRolService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()) {
            throw new ModelNotFoundException("Id not found " + request.getRolId());
        }
        tryToUpdate();
    }

    private boolean exist() {
        return service.exist(request.getRolId());
    }

    private void tryToUpdate() {
        try{
            response = update();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private RolResponse update() {
        return ComposeRol
                .builRolResponse(service
                        .update(ComposeRol
                                .composeRol(request)));
    }

}
