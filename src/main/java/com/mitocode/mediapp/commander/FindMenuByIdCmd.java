package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeMenu;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.MenuResponse;
import com.mitocode.mediapp.service.IMenuService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindMenuByIdCmd implements BusinessLogicCommand {
    private IMenuService service;

    @Setter
    private Integer id;

    @Getter
    private MenuResponse response;

    public FindMenuByIdCmd(IMenuService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = findById();
        }catch (Exception e){
            throw new ModelNotFoundException("Id not found "+ id);
        }
    }

    private MenuResponse findById() {
        return ComposeMenu.buildMenuResponse(service.findById(id));
    }

}
