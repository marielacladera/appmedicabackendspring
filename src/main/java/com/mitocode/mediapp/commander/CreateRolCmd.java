package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeRol;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.RolCreateInput;
import com.mitocode.mediapp.response.RolResponse;
import com.mitocode.mediapp.service.IRolService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateRolCmd implements BusinessLogicCommand {
    private IRolService service;

    @Setter
    private RolCreateInput request;

    @Getter
    private RolResponse response;

    public CreateRolCmd(IRolService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = create();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private RolResponse create() {
        return ComposeRol.builRolResponse(service
                .create(ComposeRol.composeCreateRol(request)));
    }

}
