package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposePatient;
import com.mitocode.mediapp.input.PatientCreateInput;
import com.mitocode.mediapp.response.PatientResponse;
import com.mitocode.mediapp.service.IPatientService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreatePatientCmd implements BusinessLogicCommand {
    private IPatientService service;

    @Setter
    private PatientCreateInput request;

    @Getter
    private PatientResponse response;

    public CreatePatientCmd(IPatientService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = create();
        } catch (Exception e) {
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    private PatientResponse create(){
        return ComposePatient
                .buildPatientResponse(
                service
                        .create(ComposePatient
                                .composeCreatePatient(request)));
    }

}
