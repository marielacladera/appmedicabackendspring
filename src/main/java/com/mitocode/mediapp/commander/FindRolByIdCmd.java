package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeRol;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.RolResponse;
import com.mitocode.mediapp.service.IRolService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindRolByIdCmd implements BusinessLogicCommand {
    private IRolService service;

    @Setter
    private Integer id;

    @Getter
    private RolResponse response;

    public FindRolByIdCmd(IRolService service){
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = findById();
        }catch (Exception e){
            throw new ModelNotFoundException("Id not found "+ id);
        }
    }

    private RolResponse findById() {
        return ComposeRol.builRolResponse(service.findById(id));
    }

}
