package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeConsultation;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.response.ConsultationResponse;
import com.mitocode.mediapp.service.serviceImpl.ConsultationService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListConsultationsCmd implements BusinessLogicCommand {

    private ConsultationService service;
    @Getter
    private List<ConsultationResponse> response;

    public ListConsultationsCmd(ConsultationService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        listConsultation();
    }

    private void listConsultation() {
        try {
            response = list();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<ConsultationResponse> list() {
        List<ConsultationResponse> list = new ArrayList<>();
        List<Consultation> listConsultation = service.list();
        for(Consultation consultation: listConsultation){
            list.add(ComposeConsultation.buildConsultationResponse(consultation));
        }
        return list;
    }
}
