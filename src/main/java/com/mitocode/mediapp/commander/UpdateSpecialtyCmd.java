package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeSpecialty;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.SpecialtyInput;
import com.mitocode.mediapp.response.SpecialtyResponse;
import com.mitocode.mediapp.service.ISpecialtyService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateSpecialtyCmd implements BusinessLogicCommand {
    private ISpecialtyService service;

    @Setter
    private SpecialtyInput request;

    @Getter
    private SpecialtyResponse response;

    public UpdateSpecialtyCmd(ISpecialtyService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()) {
            throw new ModelNotFoundException("Id not found " + request.getSpecialtyId());
        }
        tryToUpdate();
    }

    private boolean exist() {
        return service.exist(request.getSpecialtyId());
    }

    private void tryToUpdate() {
        try{
            response = update();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private SpecialtyResponse update() {
        return ComposeSpecialty
                .buildSpecialtyResponse(service
                        .update(ComposeSpecialty
                                .composeSpecialty(request)));
    }

}
