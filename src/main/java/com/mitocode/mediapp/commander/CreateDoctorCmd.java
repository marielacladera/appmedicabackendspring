package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeDoctor;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelInternalErrorException;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.DoctorCreateInput;
import com.mitocode.mediapp.response.DoctorResponse;
import com.mitocode.mediapp.service.IDoctorService;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateDoctorCmd implements BusinessLogicCommand {
    private IDoctorService service;

    @Setter
    private DoctorCreateInput request;

    @Getter
    private DoctorResponse response;

    public CreateDoctorCmd(IDoctorService service) {
        this.service = service;
    }

    @SneakyThrows
    @Override
    public void execute() {
        try{
            response = create();
        }catch (Exception e) {
            new ModelInternalErrorException("Sorry, I am an error of internal server!!");
        }
    }

    private DoctorResponse create() {
        return ComposeDoctor
                .buildDoctorResponse(service
                        .create(ComposeDoctor
                                .composeCreateDoctor(request)));
    }

}
