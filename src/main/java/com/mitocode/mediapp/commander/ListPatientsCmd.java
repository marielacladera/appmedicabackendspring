package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposePatient;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.domain.Patient;
import com.mitocode.mediapp.response.PatientResponse;
import com.mitocode.mediapp.service.IPatientService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListPatientsCmd implements BusinessLogicCommand {

    private IPatientService service;

    @Getter
    private List<PatientResponse> response;

    public ListPatientsCmd(IPatientService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = list();
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500,e);
        }
    }

    private List<PatientResponse> list() {
        List<Patient> patients = service.list();
        return patients.stream()
                .map(item -> ComposePatient.buildPatientResponse(item))
                .collect(Collectors.toList());
    }

}
