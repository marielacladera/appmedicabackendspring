package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeExam;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.ExamResponse;
import com.mitocode.mediapp.service.IExamService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindExamByIdCmd implements BusinessLogicCommand {
    private IExamService service;

    @Setter
    private Integer id;

    @Getter
    private ExamResponse response;

    public FindExamByIdCmd(IExamService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = findById();
        }catch (Exception e) {
            throw new ModelNotFoundException("Id not found " + id);
        }
    }

    private ExamResponse findById() {
        return ComposeExam.builExamResponse(service.findById(id));
    }

}
