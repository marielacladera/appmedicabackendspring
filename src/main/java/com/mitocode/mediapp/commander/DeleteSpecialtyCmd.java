package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.service.ISpecialtyService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteSpecialtyCmd implements BusinessLogicCommand {
    private ISpecialtyService service;

    @Setter
    private Integer id;

    public DeleteSpecialtyCmd(ISpecialtyService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()){
            throw new ModelNotFoundException("ID not found "+ id);
        }
        delete();
    }

    private boolean exist() {
        return service.exist(id);
    }

    private void delete() {
        try {
            service.delete(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

}
