package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeSpecialty;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.SpecialtyCreateInput;
import com.mitocode.mediapp.response.SpecialtyResponse;
import com.mitocode.mediapp.service.ISpecialtyService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateSpecialtyCmd implements BusinessLogicCommand {
    private ISpecialtyService service;

    @Setter
    private SpecialtyCreateInput request;

    @Getter
    private SpecialtyResponse response;

    public CreateSpecialtyCmd(ISpecialtyService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = create();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e );
        }
    }

    private SpecialtyResponse create() {
        return ComposeSpecialty
                .buildSpecialtyResponse(service
                        .create(ComposeSpecialty
                                .composeCreateSpecialty(request)));
    }

}
