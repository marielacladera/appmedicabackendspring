package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.service.serviceImpl.UserService;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateUserCmd implements BusinessLogicCommand {
    private UserService service;

    public CreateUserCmd(UserService service) {
        this.service = service;
    }

    @Override
    public void execute() {

    }
}
