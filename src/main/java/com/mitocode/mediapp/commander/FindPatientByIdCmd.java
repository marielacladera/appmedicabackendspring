package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposePatient;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.PatientResponse;
import com.mitocode.mediapp.service.IPatientService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindPatientByIdCmd implements BusinessLogicCommand {
    private IPatientService service;

    @Setter
    private Integer id;

    @Getter
    private PatientResponse response;

    public FindPatientByIdCmd(IPatientService service) {
        this.service =service;
    }

    @Override
    public void execute() {
        try{
            response = findById();
        }catch (Exception e){
            throw new ModelNotFoundException("Id not found "+ id);
        }
    }

    private PatientResponse findById() {
        return ComposePatient.buildPatientResponse(service.findById(id));
    }

}
