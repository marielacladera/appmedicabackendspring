package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeDoctor;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.DoctorResponse;
import com.mitocode.mediapp.service.IDoctorService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindDoctorByIdCmd implements BusinessLogicCommand {
    private IDoctorService service;

    @Setter
    private Integer id;

    @Getter
    private DoctorResponse response;

    public FindDoctorByIdCmd(IDoctorService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = findById();
        }catch (Exception e) {
            throw new ModelNotFoundException("Id not found " + id);
        }
    }

    private DoctorResponse findById() {
         return ComposeDoctor
                 .buildDoctorResponse(service.findById(id));
    }
}
