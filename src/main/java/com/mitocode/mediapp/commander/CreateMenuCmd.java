package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeMenu;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.MenuCreateInput;
import com.mitocode.mediapp.response.MenuResponse;
import com.mitocode.mediapp.service.IMenuService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateMenuCmd implements BusinessLogicCommand {
    private IMenuService service;

    @Setter
    private MenuCreateInput request;

    @Getter
    private MenuResponse response;

    public CreateMenuCmd(IMenuService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = create();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private MenuResponse create(){
        return ComposeMenu
                .buildMenuResponse(service
                        .create(ComposeMenu
                                .composeCreateMenu(request)));
    }
}
