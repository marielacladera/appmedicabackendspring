package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposePatient;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.PatientInput;
import com.mitocode.mediapp.response.PatientResponse;
import com.mitocode.mediapp.service.IPatientService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdatePatientCmd implements BusinessLogicCommand {
    private IPatientService service;

    @Setter
    private PatientInput request;

    @Getter
    private PatientResponse response;

    public UpdatePatientCmd(IPatientService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()) {
            throw new ModelNotFoundException("Id not found " + request.getPatientId());
        }
        tryToUpdate();
    }

    private boolean exist() {
        return service.exist(request.getPatientId());
    }

    private void tryToUpdate() {
        try{
            response = update();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private PatientResponse update() {
        return ComposePatient
                .buildPatientResponse(service
                        .update(ComposePatient
                                .composePatient(request)));
    }

}
