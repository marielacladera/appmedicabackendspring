package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeSpecialty;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.SpecialtyResponse;
import com.mitocode.mediapp.service.ISpecialtyService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListSpecialtiesCmd implements BusinessLogicCommand {
    private ISpecialtyService service;

    @Getter
    private List<SpecialtyResponse> response;

    public ListSpecialtiesCmd(ISpecialtyService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = list();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<SpecialtyResponse> list() {
        return service.list().stream()
                .map(item -> ComposeSpecialty.buildSpecialtyResponse(item))
                .collect(Collectors.toList());
    }

}
