package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeDoctor;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.DoctorResponse;
import com.mitocode.mediapp.service.IDoctorService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListDoctorsCmd implements BusinessLogicCommand {
    private IDoctorService service;

    @Getter
    private List<DoctorResponse> response;

   public ListDoctorsCmd(IDoctorService service) {
       this.service = service;
   }

    @Override
    public void execute() {
        try {
            response = list();
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<DoctorResponse> list() {
       return service.list().stream()
               .map(item -> ComposeDoctor.buildDoctorResponse(item))
               .collect(Collectors.toList());
    }
}
