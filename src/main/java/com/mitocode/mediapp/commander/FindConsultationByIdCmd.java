package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeConsultation;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.ConsultationResponse;
import com.mitocode.mediapp.service.serviceImpl.ConsultationService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindConsultationByIdCmd implements BusinessLogicCommand {

    private ConsultationService service;

    @Setter
    private Integer id;

    @Getter
    private ConsultationResponse response;

    public FindConsultationByIdCmd(ConsultationService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        findConsultationById();
    }

    private void findConsultationById() {
        try {
            response = findById();
        }catch (Exception e){
            throw new ModelNotFoundException("Id not found "+ id);
        }
    }

    private ConsultationResponse findById() {
        return ComposeConsultation.buildConsultationResponse(service.findById(id));
    }
}
