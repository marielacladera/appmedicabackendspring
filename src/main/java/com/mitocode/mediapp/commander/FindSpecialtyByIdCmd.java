package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeSpecialty;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.response.SpecialtyResponse;
import com.mitocode.mediapp.service.ISpecialtyService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindSpecialtyByIdCmd implements BusinessLogicCommand {
    private ISpecialtyService service;

    @Setter
    private Integer id;

    @Getter
    private SpecialtyResponse response;

    public FindSpecialtyByIdCmd(ISpecialtyService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = findById();
        }catch (Exception e){
            throw new ModelNotFoundException("Id not found "+ id);
        }
    }

    private SpecialtyResponse findById() {
        return ComposeSpecialty.buildSpecialtyResponse(service.findById(id));
    }

}
