package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeMenu;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.MenuResponse;
import com.mitocode.mediapp.service.IMenuService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListMenusCmd implements BusinessLogicCommand {
    private IMenuService service;

    @Getter
    private List<MenuResponse> response;

    public ListMenusCmd(IMenuService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = list();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<MenuResponse> list() {
        return service.list().stream()
                .map(item -> ComposeMenu.buildMenuResponse(item))
                .collect(Collectors.toList());
    }

}
