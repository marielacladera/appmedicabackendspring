package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeExam;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.input.ExamCreateInput;
import com.mitocode.mediapp.response.ExamResponse;
import com.mitocode.mediapp.service.IExamService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateExamCmd implements BusinessLogicCommand {
    private IExamService service;

    @Setter
    private ExamCreateInput request;

    @Getter
    private ExamResponse response;

    public CreateExamCmd(IExamService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = create();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e );
        }
    }

    private ExamResponse create() {

        return ComposeExam
                .builExamResponse(service
                        .create(ComposeExam
                                .composeCreateExam(request)));
    }

}
