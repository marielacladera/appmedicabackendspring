package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeConsultation;
import com.mitocode.mediapp.builder.ComposeExam;
import com.mitocode.mediapp.domain.Consultation;
import com.mitocode.mediapp.domain.Exam;
import com.mitocode.mediapp.input.ConsultationListExamCreateInput;
import com.mitocode.mediapp.service.serviceImpl.ConsultationService;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateConsultationCmd implements BusinessLogicCommand {
    private ConsultationService service;

    @Setter
    private ConsultationListExamCreateInput request;

    @Getter
    private Consultation response;

    public CreateConsultationCmd(ConsultationService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        Consultation consultation = ComposeConsultation.composeConsultation(request.getConsultation());
        List<Exam> examList = request.getExams().stream().map(exam -> ComposeExam.composeExam(exam)).collect(Collectors.toList());

        response = recordTransaction(consultation, examList);
    }

    private Consultation recordTransaction(Consultation consultation, List<Exam> examList) {
        return service.recordTransaction(consultation, examList);
    }

}
