package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeRol;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.RolResponse;
import com.mitocode.mediapp.service.IRolService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListRolesCmd implements BusinessLogicCommand {
    private IRolService service;

    @Getter
    private List<RolResponse> response;

    public ListRolesCmd(IRolService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try {
            response = list();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<RolResponse> list() {
        return service.list().stream()
                .map(item -> ComposeRol.builRolResponse(item))
                .collect(Collectors.toList());
    }

}
