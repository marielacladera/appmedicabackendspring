package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeMenu;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.MenuInput;
import com.mitocode.mediapp.response.MenuResponse;
import com.mitocode.mediapp.service.IMenuService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateMenuCmd implements BusinessLogicCommand {
    private IMenuService service;

    @Setter
    private MenuInput request;

    @Getter
    private MenuResponse response;

    public UpdateMenuCmd(IMenuService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()){
            throw new ModelNotFoundException("Id not found " + request.getMenuId());
        }
        tryToUpdate();
    }

    private boolean exist() {
        return service.exist(request.getMenuId());
    }

    private void tryToUpdate() {
        try{
            response = update();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private MenuResponse update() {
        return ComposeMenu
                .buildMenuResponse(service
                        .update(ComposeMenu
                            .composeMenu(request)));
    }

}
