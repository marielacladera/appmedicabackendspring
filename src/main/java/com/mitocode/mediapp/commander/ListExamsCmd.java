package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeExam;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.response.ExamResponse;
import com.mitocode.mediapp.service.IExamService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListExamsCmd implements BusinessLogicCommand {
    private IExamService service;

    @Getter
    private List<ExamResponse> response;

    public ListExamsCmd(IExamService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        try{
            response = list();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<ExamResponse> list() {
        return service.list().stream()
                .map(item -> ComposeExam.builExamResponse(item))
                .collect(Collectors.toList());
    }
}
