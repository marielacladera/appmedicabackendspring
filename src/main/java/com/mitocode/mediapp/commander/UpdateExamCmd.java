package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeExam;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.ExamInput;
import com.mitocode.mediapp.response.ExamResponse;
import com.mitocode.mediapp.service.IExamService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateExamCmd implements BusinessLogicCommand {
    private IExamService service;

    @Setter
    private ExamInput request;

    @Getter
    private ExamResponse response;

    public UpdateExamCmd(IExamService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()){
            throw new ModelNotFoundException("Id not found " + request.getExamId());
        }
        tryToUpdate();
    }

    private boolean exist(){
        return service.exist(request.getExamId());
    }

    private void tryToUpdate() {
        try{
            response = update();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private ExamResponse update() {
        return ComposeExam
                .builExamResponse(service
                        .update(ComposeExam
                                .composeExam(request)));
    }

}
