package com.mitocode.mediapp.commander;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.mitocode.mediapp.builder.ComposeDoctor;
import com.mitocode.mediapp.constants.Constants;
import com.mitocode.mediapp.exception.ModelNotFoundException;
import com.mitocode.mediapp.input.DoctorInput;
import com.mitocode.mediapp.response.DoctorResponse;
import com.mitocode.mediapp.service.IDoctorService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateDoctorCmd implements BusinessLogicCommand {
    private IDoctorService service;

    @Setter
    private DoctorInput request;

    @Getter
    private DoctorResponse response;

    public UpdateDoctorCmd(IDoctorService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!exist()){
            throw new ModelNotFoundException("ID not found "+ request.getDoctorId());
        }
        tryToUpdate();
    }

    private boolean exist() {
        return service.exist(request.getDoctorId());
    }

    private void tryToUpdate() {
        try {
            response = update();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private DoctorResponse update() {
        return ComposeDoctor
                .buildDoctorResponse(service
                        .updateValuesDoctor(ComposeDoctor
                                .composeDoctor(request)));
    }
}
