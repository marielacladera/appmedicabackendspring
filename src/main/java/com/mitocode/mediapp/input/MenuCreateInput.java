package com.mitocode.mediapp.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class MenuCreateInput {

    @NotNull(message = "name: the field must not be null")
    @NotEmpty(message = "names the field must not be empty")
    @Size(min =3, max = 60, message = "name: the field must have between three and seventy characters")
    private String name;

    @NotNull(message = "url: the field must not be null")
    @NotEmpty(message = "url: the field must not be empty")
    @Size(min = 3, max = 150, message = "url: the field must have between three and one hundred fifty characters")
    private String url;


    @NotNull(message = "icon: the field must not be null")
    @NotEmpty(message = "icon: the field must not be empty")
    @Size(min =3, max = 30, message = "icon: the field must have between three and thirty characters")
    private String icon;

    private List<RolInput> roles;

}
