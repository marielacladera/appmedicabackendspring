package com.mitocode.mediapp.input;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class DetailConsultationInput {

    @NotNull
    private String diagnosis;

    @NotNull
    private String treatment;

}
