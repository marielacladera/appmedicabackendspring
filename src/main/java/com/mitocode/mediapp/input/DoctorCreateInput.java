package com.mitocode.mediapp.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */
@Schema(description = "Request to create a doctor")
@Getter
public class DoctorCreateInput {

    @NotNull(message = "names: the field must not be null")
    @NotEmpty(message = "names: the field must not be empty")
    @Size(min =3, message = "names: the field minimum must have three characters")
    private String names;

    @NotNull(message = "lastName: the field must not be null")
    @NotEmpty(message = "lastName: the field must not be empty")
    private String lastName;

    @NotNull(message = "cmp: the field must not be null")
    @NotEmpty(message = "cmp: the field must not be empty")
    @Size(max = 20, message = "cmp: the field maximun must have 20 characters")
    private String cmp;

    @NotNull(message = "cmp: the field must not be null")
    @NotEmpty(message = "cmp: the field must not be empty")
    private String urlPhoto;

}
