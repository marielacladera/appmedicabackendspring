package com.mitocode.mediapp.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class SpecialtyCreateInput {

    @NotNull(message = "name: the field must not be null")
    @NotEmpty(message = "names the field must not be empty")
    private String name;

    @NotNull(message = "description: the field must not be null")
    @NotEmpty(message = "description: the field must not be empty")
    private String description;

}
