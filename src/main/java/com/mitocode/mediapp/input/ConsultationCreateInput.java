package com.mitocode.mediapp.input;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class ConsultationCreateInput {

    @NotNull
    private DoctorInput doctor;

    @NotNull
    private PatientInput patient;

    @NotNull
    private SpecialtyInput specialty;

    @NotNull
    private LocalDateTime date;

    @NotNull
    private String officeNumber;

    @NotNull
    private List<DetailConsultationInput> detailsConsultation;

}
