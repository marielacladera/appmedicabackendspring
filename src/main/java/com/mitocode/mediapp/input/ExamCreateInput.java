package com.mitocode.mediapp.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class ExamCreateInput {

    @NotNull(message = "name: the field must not be null")
    @Size(min =3, message = "name: the field must have between three and seventy characters")
    private String name;

    @NotNull(message = "description: the field must not be null")
    @NotEmpty(message = "description: the field must not be empty")
    @Size(min = 3, max = 200, message = "description: the field must have between three and two hundred characters")
    private String description;

}
