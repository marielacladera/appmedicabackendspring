package com.mitocode.mediapp.input;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */

@Getter
public class PatientCreateInput {

    @NotNull(message = "names: the field must not be null")
    @NotEmpty(message = "names: the field must not be empty")
    @Size(min = 3, max= 70, message = "names: the field minimum must have six characters")
    private String names;

    @NotNull(message = "lastName: the field must not be null")
    @NotEmpty(message = "lastName: the field must not be empty")
    @Size(min = 3, max = 70, message = "lastName: the field minimum must have six characters")
    private String lastName;

    @NotNull(message = "dni: the field must not be null")
    @NotEmpty(message = "dni: the field must not be empty")
    @Size(min = 6, max = 12, message = "dni: the field minimum must have six characters")
    private String dni;

    @NotNull(message = "address: the field must not be null")
    @NotEmpty(message = "address: the field must not be empty")
    @Size(min=3, max = 60, message = "address: the field minimum must have 3 between 150 characters")
    private String address;

    @NotNull(message = "phone: the field must not be null")
    @NotEmpty(message = "phone: the field must not be empty")
    @Size(min= 3, max = 20, message = "phone: the field minimum must have six characters")
    private String phone;

    @NotNull(message = "email: the field must not be null")
    @NotEmpty(message = "email: the field must not be empty")
    @Size(max = 50, message = "dni: the field minimum must have six characters")
    @Email
    private String email;

}
