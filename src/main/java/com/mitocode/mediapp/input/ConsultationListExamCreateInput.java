package com.mitocode.mediapp.input;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class ConsultationListExamCreateInput {

    @NotNull
    private ConsultationCreateInput consultation;

    @NotNull
    private List<ExamInput> exams;
}
