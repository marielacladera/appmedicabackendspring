package com.mitocode.mediapp.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class RolCreateInput {

    @NotNull(message = "name: the field must not be null")
    @NotEmpty(message = "name: the field must not be empty")
    @Size(min =3, max = 15, message = "name: the field must have between three and fifteen characters")
    private String name;

    @NotNull(message = "description: the field must not be null")
    @NotEmpty(message = "description: the field must not be empty")
    @Size(min = 3, max = 150, message = "description: the field must have between three and one hundred fifty characters")
    private String description;

}
