package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class MenuResponse {

    private Integer menuId;

    private String name;

    private String url;

    private String icon;

    private List<RolResponse> roles;
}
