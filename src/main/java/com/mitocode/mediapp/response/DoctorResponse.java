package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class DoctorResponse {

    private Integer doctorId;

    private String names;

    private String lastName;

    private String cmp;

    private String urlPhoto;

}
