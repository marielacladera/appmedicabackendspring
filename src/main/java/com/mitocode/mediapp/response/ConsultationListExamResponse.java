package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class ConsultationListExamResponse {

    private ConsultationResponse consultation;

    private List<ExamResponse> exams;

}
