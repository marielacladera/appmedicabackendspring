package com.mitocode.mediapp.response;

import com.mitocode.mediapp.input.DetailConsultationInput;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class ConsultationResponse {

    private Integer consultationId;

    private DoctorResponse doctor;

    private PatientResponse patient;

    private SpecialtyResponse specialty;

    private LocalDateTime date;

    private String officeNumber;

    private List<DetailConsultationResponse> detailsConsultation;

}
