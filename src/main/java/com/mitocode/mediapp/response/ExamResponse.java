package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class ExamResponse {

    private Integer examId;

    private String name;

    private String description;

}
