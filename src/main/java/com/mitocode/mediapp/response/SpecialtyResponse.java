package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class SpecialtyResponse {

    private Integer specialtyId;

    private String name;

    private String description;
}
