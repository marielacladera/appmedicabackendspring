package com.mitocode.mediapp.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mitocode.mediapp.input.ConsultationCreateInput;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class DetailConsultationResponse {

    private Integer detailId;

    @JsonIgnore
    private ConsultationResponse consultation;

    @NotNull
    private String diagnosis;

    @NotNull
    private String treatment;
}
