package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class PatientResponse {

    private Integer patientId;

    private String names;

    private String lastName;

    private String dni;

    private String address;

    private String phone;

    private String email;

}
