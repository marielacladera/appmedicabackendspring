package com.mitocode.mediapp.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class RolResponse {

    private Integer rolId;

    private String name;

    private String description;

    private List<RolResponse> roles;
}
