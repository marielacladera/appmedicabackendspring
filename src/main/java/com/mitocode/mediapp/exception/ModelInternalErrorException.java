package com.mitocode.mediapp.exception;

/**
 * @author Mariela Cladera M.
 */
public class ModelInternalErrorException extends Exception {

    private static final long serialVersionUID = 1L;

    public ModelInternalErrorException(String mensaje) {
        super(mensaje);
    }
}
