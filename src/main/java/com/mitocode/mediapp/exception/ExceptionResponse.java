package com.mitocode.mediapp.exception;

import lombok.*;

import java.time.LocalDateTime;

/**
 * @author Mariela Cladera M.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionResponse {

    private LocalDateTime fecha;

    private String mensaje;

    private String detalles;

}
