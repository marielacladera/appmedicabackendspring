package com.mitocode.mediapp;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
		info = @Info(title = "Medic Application",
				version = "0.0.0",
				description = "Application to register consults of patients.",
				license = @License(
						name = "SoftRed",
						url = "http://softred.com"),
				contact = @Contact(url = "http://softred.contact.com",
						name = "Mariela",
						email = "marielacladera@gmail.com")
		)
)
@SpringBootApplication
public class MediappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediappApplication.class, args);
	}

}
