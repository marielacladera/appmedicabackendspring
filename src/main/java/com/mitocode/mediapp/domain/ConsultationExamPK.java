package com.mitocode.mediapp.domain;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Embeddable
public class ConsultationExamPK implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "consultation_id", nullable = false)
    private Consultation consultation;

    @ManyToOne
    @JoinColumn(name = "exam_id", nullable = false)
    private Exam exam;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsultationExamPK that = (ConsultationExamPK) o;
        return consultation.equals(that.consultation) && exam.equals(that.exam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consultation, exam);
    }

}
