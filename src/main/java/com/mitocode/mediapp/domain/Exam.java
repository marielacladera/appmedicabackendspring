package com.mitocode.mediapp.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer examId;

    @Column(nullable = false, length = 70)
    private String name;

    @Column(nullable = false, length = 200)
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exam exam = (Exam) o;
        return examId.equals(exam.examId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(examId);
    }
}
