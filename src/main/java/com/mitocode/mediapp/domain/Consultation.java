package com.mitocode.mediapp.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Consultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer consultationId;

    @ManyToOne
    @JoinColumn(name= "patient_id", nullable = false, foreignKey = @ForeignKey(name= "FK_consultation_patient"))
    private Patient patient;

    @ManyToOne
    @JoinColumn(name= "doctor_id", nullable = false, foreignKey = @ForeignKey(name= "FK_consultation_doctor"))
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name= "specialty_id", nullable = false, foreignKey = @ForeignKey(name= "FK_consultation_specialty"))
    private Specialty specialty;

    @OneToMany(mappedBy = "consultation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DetailConsultation> detailConsultation;

    private LocalDateTime date;

    private String officeNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consultation that = (Consultation) o;
        return consultationId.equals(that.consultationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consultationId);
    }

}
