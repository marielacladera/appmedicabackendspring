package com.mitocode.mediapp.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
@IdClass(ConsultationExamPK.class)
public class ConsultationExam {

    @Id
    private Consultation consultation;

    @Id
    private Exam exam;

}
