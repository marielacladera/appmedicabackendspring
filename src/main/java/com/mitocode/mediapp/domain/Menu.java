package com.mitocode.mediapp.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer menuId;

    @Column(nullable = false, length = 60)
    private String name;

    @Column(nullable = false, length = 150)
    private String url;

    @Column(nullable = false, length = 30)
    private String icon;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "menu_rol", joinColumns = @JoinColumn(name = "menu_id", referencedColumnName = "menuId"),
        inverseJoinColumns = @JoinColumn(name = "rol_id", referencedColumnName = "rolId"))
    private List<Rol> roles;

}
