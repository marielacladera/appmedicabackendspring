package com.mitocode.mediapp.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class DetailConsultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer detailId;

    @ManyToOne
    @JoinColumn(name = "consultation_id", nullable = false, foreignKey = @ForeignKey(name = "FK_consultation_detail"))
    private Consultation consultation;

    @Column(nullable = false)
    private String diagnosis;

    @Column(nullable = false)
    private String treatment;

}
