package com.mitocode.mediapp.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer doctorId;

    @Column(nullable = false, length = 70)
    private String names;

    @Column(nullable = false, length = 70)
    private String lastName;

    @Column(nullable = false, length = 20, unique = true)
    private String cmp;

    @Column(nullable = false)
    private String urlPhoto;

}
